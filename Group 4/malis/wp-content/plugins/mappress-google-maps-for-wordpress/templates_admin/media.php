<div class='mapp-mce'>
	<div class='mapp-mce-header'>
		<?php echo Mappress::get_support_links(); ?>
	</div>
	<div class='mapp-mce-list-panel'>
		<div class='mapp-mce-list-toolbar'>
			<input data-mapp-mce='add' class='button button-primary' type='button' value='<?php esc_attr_e('New Map', 'mappress-google-maps-for-wordpress')?>' />
			<select class='mapp-mce-list-type'>
				<option value='post'><?php _e('This post', 'mappress-google-maps-for-wordpress');?></option>
				<option value='all'><?php _e('All posts', 'mappress-google-maps-for-wordpress');?></option>
			</select>
			<input type='text' size="15" class='mapp-mce-search' placeholder='<?php _e('Filter by title', 'mappress-google-maps-for-wordpress');?>'>
			<span class='spinner'></span>
		</div>
		<div class='mapp-mce-list'></div>
	</div>

	<div class='mapp-mce-edit-panel'>
		<table class='mapp-settings'>
			<tr>
				<td><?php _e('Map ID', 'mappress-google-maps-for-wordpress');?>:</td>
				<td><span class='mapp-mce-mapid'></span></td>
			</tr>

			<tr>
				<td><?php _e('Map Title', 'mappress-google-maps-for-wordpress');?>:</td>
				<td><input class='mapp-mce-title' type='text' placeholder='<?php _e('Untitled', 'mappress-google-maps-for-wordpress');?>' /></td>
			</tr>

			<tr>
				<td><?php _e('Display Size', 'mappress-google-maps-for-wordpress');?>:</td>
				<td>
					<?php
						$sizes = array();
						foreach(Mappress::$options->sizes as $i => $size)
							$sizes[] = "<a href='#' class='mapp-mce-size' data-width='{$size['width']}' data-height='{$size['height']}'>" . $size['width'] . 'x' . $size['height'] . "</a>";
						echo implode(' | ', $sizes);
					?>
					<input type='text' class='mapp-mce-width' size='2' value='' /> x <input type='text' class='mapp-mce-height' size='2' value='' />
				</td>
			</tr>

			<tr>
				<td><?php _e('Save center / zoom', 'mappress-google-maps-for-wordpress');?></td>
				<td><input type='checkbox' class='mapp-mce-viewport'></td>
			</tr>
		</table>
		<div class='mapp-mce-edit-toolbar'>
			<input data-mapp-mce='save' class='button button-primary' type='button' value='<?php esc_attr_e('Save', 'mappress-google-maps-for-wordpress'); ?>' />
			<input data-mapp-mce='cancel' class='button' type='button' value='<?php esc_attr_e('Cancel', 'mappress-google-maps-for-wordpress'); ?>' />
			<input data-mapp-mce='insert' class='button' type='button' value='<?php esc_attr_e('Insert into post', 'mappress-google-maps-for-wordpress'); ?>' />
		</div>
        <div class='mapp-edit'></div>
	</div>
</div>

<script type='text/template' id='mapp-tmpl-mce-list'>
	<div class='mapp-mce-items'>
		<# _.forEach(items, function(item, i) { #>
			<div class='mapp-mce-item' data-mapp-mce-list='edit' data-mapp-mapid='{{ item.mapid }}'>

				<# if (type == 'all') { #>
					<# if (item.post_title) { #>{{ item.post_title }}<# } else { #><?php _e('Untitled', 'mappress-google-maps-for-wordpress');?><# } #>
					&nbsp;-&nbsp;
				<# } #>

				<# if (item.map_title) { #>{{ item.map_title }}<# } else { #><?php _e('Untitled', 'mappress-google-maps-for-wordpress');?><# } #>

				<div class='mapp-actions'>
					<a href='#' data-mapp-mce-list='edit'><?php _e('Edit', 'mappress-google-maps-for-wordpress');?></a> |&nbsp;
					<a href='#' data-mapp-mce-list='insert'><?php _e('Insert into post', 'mappress-google-maps-for-wordpress');?></a> |&nbsp;
					<a href='#' data-mapp-mce-list='remove'><?php _e('Delete', 'mappress-google-maps-for-wordpress');?></a>
				</div>
			</div>
		<# }); #>
	</div>
	<div class='mapp-list-footer'>
		<# if (items.length == 0) { #>
			<?php _e('No maps found', 'mappress-google-maps-for-wordpress');?>
		<# } #>
	</div>
</div>
</script>